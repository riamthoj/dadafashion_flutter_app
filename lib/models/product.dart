
import 'package:json_annotation/json_annotation.dart';
part 'product.json.dart';
@JsonSerializable()
class ProductModel{
  @JsonKey(name:"pro_id",nullable: true)
  String? pro_id;
  @JsonKey(name:"pro_name",nullable: true)
  String? pro_name;
  @JsonKey(name:"category",nullable: true)
  String? category;
  @JsonKey(name:"cate_id",nullable: true)
  int? cate_id;
  @JsonKey(name:"unit",nullable: true)
  String? unit;
  @JsonKey(name:"unit_id",nullable: true)
  int? unit_id;
  @JsonKey(name:"color",nullable: true)
  String? color;
  @JsonKey(name:"size",nullable: true)
  String? size;
  @JsonKey(name:"price",nullable: true)
  double? price;
  @JsonKey(name:"qty",nullable: true)
  int? qty;
  @JsonKey(name:"created_date",nullable: true)
  String? created_date;
  @JsonKey(name:"image",nullable: true)
  String? image;
ProductModel({
  this.pro_id,
  this.pro_name,
  this.category,
  this.cate_id,
  this.unit,
  this.unit_id,
  this.color,
  this.size,
  this.price,
  this.qty,
  this.created_date,
  this.image
});
factory ProductModel.fromJson(Map<String, dynamic> json) {
     return _$ProductFromJson(json);
}
  Map<String, dynamic> toJson() => _$ProductToJson(this);
}