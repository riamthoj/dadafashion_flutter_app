part of 'product.dart';
// **************************
// JsonSerializableGenerator
// **************************;


ProductModel _$ProductFromJson(Map<String, dynamic> json) {
  return ProductModel(
    pro_id: json['pro_id'] as String?,
    pro_name: json['pro_name'] as String?,
    category: json['category'] as String?,
    cate_id: json['cate_id'] as int?,
    unit: json['unit'] as String?,
    unit_id: json['unit_id'] as int?,
    color: json['color'] as String?,
    size: json['size'] as String?,
    price: json['price'] as double?,
    qty: json['qty'] as int?,
    created_date: json['created_date'] as String?,
    image: json['image'] as String?,
  );
}

Map<String, dynamic> _$ProductToJson(ProductModel instance) => <String, dynamic>{
      'pro_id': instance.pro_id,
      'pro_name': instance.pro_name,
      'category': instance.category,
      'cate_id': instance.cate_id,
      'unit': instance.unit,
      'unit_id': instance.unit_id,
      'color': instance.color,
      'size': instance.size,
      'price': instance.price,
      'qty': instance.qty,
      'created_date': instance.created_date,
      'image': instance.image,
    };