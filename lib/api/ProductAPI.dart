
import 'package:dadafashion_app/api/API.dart';
import 'package:dadafashion_app/config/constant.dart';
import 'package:dadafashion_app/models/product.dart';
import 'package:http/http.dart' as http;

class ProductAPI extends DadaFashionAPI with Product{
@override
Future getProducts() async{
  final response=await http.get(Uri.http(baseUrl, "/product"),headers: header);
  try {
    final res=response.body;
    final List<dynamic> ? mapResult=decoder!.convert(res);
    List<ProductModel>? product=[];
    mapResult!.forEach((el) {
      product.add(ProductModel.fromJson(el));
     });
     return product;
  } catch (e) {
    return e;
  }
}
@override
Future getProduct({String? id}) async{
  final res=await http.get(Uri.http(baseUrl, "/product"),headers: header);
}
@override
 Future createProduct({String? id,String? name, String? category, String? unit, String? size,String? color,double? price, int? qty,String? image})async{

  final res=await http.get(Uri.http(baseUrl, "/product"),headers: header);
 }
@override
  Future updateProduct({String? id}) async{
  final res=await http.get(Uri.http(baseUrl, "/product"),headers: header);

  }
@override
  Future deleteProduct({String? id}) async{
  final res=await http.get(Uri.http(baseUrl, "/product"),headers: header);

  }
}