import 'dart:convert';

class DadaFashionAPI{
  final JsonDecoder? decoder=JsonDecoder();
}

abstract class Product{
  Future getProducts();
  Future getProduct({String? id});
  Future createProduct({String? id,String? name, String? category, String? unit, String? size,String? color,double? price, int? qty,String? image});
  Future updateProduct({String? id});
  Future deleteProduct({String? id});
}