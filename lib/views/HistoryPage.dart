
import 'package:dadafashion_app/router/router_name.dart';
import 'package:flutter/material.dart';

class HistoryPage extends StatefulWidget {
  @override
  _HistoryPageState createState() => _HistoryPageState();
}

class _HistoryPageState extends State<HistoryPage> {
  @override
  Widget build(BuildContext context) {
   return Scaffold(
      appBar:  new AppBar(
        title: Text( "History"),
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back,
          ),
          onPressed: (){
            Navigator.pushNamedAndRemoveUntil(context, homeRoute,(route)=>false);
          },
        ),
      ),
      body:ListView(
        children: <Widget>[
           Container(
            child: Center(
              child: Text('History',style: TextStyle(fontSize: 18,color: Colors.deepOrange),),
            )
          ),
        ],
      ),
    );
  }
}
