import 'package:dadafashion_app/router/router_name.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        // leading: IconButton(onPressed:() =>Scaffold.of(context).openDrawer() , 
        // icon: Icon(Icons.menu_sharp,color: Colors.white)),
        title: Text(
          'Home Page',
          style: TextStyle(
              fontSize: 20, fontFamily: 'LaoFont', fontWeight: FontWeight.bold, ),
        ),
        centerTitle: true,
        backgroundColor: Colors.teal,
        foregroundColor: Colors.white,
        actions: <Widget>[
         
          Stack(
            children: [ IconButton(
            icon: const Icon(Icons.notifications_none),
            iconSize: 28.0,
            color: Colors.orange,
            onPressed: () {
              Navigator.pushNamed(context, notificatRoute);
            },
          ),
          Align(
            alignment: Alignment.topRight,
            child: Text('99',style: TextStyle(color: Colors.orange)))],
          ),
        
          Stack(
            children: [IconButton(
            icon: const Icon(Icons.shopping_cart_sharp),
            iconSize: 28.0,
            color: Colors.white,
            onPressed: () {
              Navigator.pushNamed(context, cartRoute);
            },
          ),
          Align(
            alignment: Alignment.topRight,
            child: Text('9',style: TextStyle(color: Colors.white)))],
          )
        
        ],
      ),
      backgroundColor: Colors.teal,
      drawerScrimColor: Colors.tealAccent,
      drawer: Drawer(
        semanticLabel: 'Drawer Navigation Bar',
        child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            DrawerHeader(
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.centerLeft,
                  colors: [Colors.tealAccent, Colors.teal],
                ),
              ),
              child: Container(
                color: Colors.transparent,
                child: Stack(
                  children: <Widget>[
                    Material(
                        color: Colors.transparent,
                        child: Align(
                            alignment: Alignment.center,
                            child: Container(
                                width: MediaQuery.of(context).size.width,
                                height:
                                    MediaQuery.of(context).size.height / 3.3,
                                child: Center(
                                  child: SvgPicture.asset('images/maker.svg',
                                      semanticsLabel: 'A red up arrow'),
                                )))),
                    Padding(
                      padding: EdgeInsets.only(top: 100),
                      child: Text(
                        'Dada Fashion',
                        style: TextStyle(color: Colors.orange, fontSize: 30.0),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            drawerTitle(Icons.home, 'Home', () {
              Navigator.pop(context);
              Navigator.pushNamed(context, homeRoute);
            }),
            drawerTitle(Icons.shopping_basket, 'Product', () {
              Navigator.pop(context);
              Navigator.pushNamed(context, productRoute);
            }),
            drawerTitle(Icons.cut_sharp, 'Order cut', () {
              Navigator.pop(context);
              Navigator.pushNamed(context, orderCutRoute);
            }),
            drawerTitle(Icons.history_sharp, 'History', () {
              Navigator.pop(context);
              Navigator.pushNamed(context, historyRoute);
            }),
            drawerTitle(Icons.star_sharp, 'Love', () {
              Navigator.pop(context);
              Navigator.pushNamed(context, loveRoute);
            }),
            drawerTitle(Icons.account_circle_sharp, 'Profile', () {
              Navigator.pop(context);
              Navigator.pushNamed(context, profileRoute);
            }),
            drawerTitle(Icons.help, 'Helpper', () {
              Navigator.pop(context);
              Navigator.pushNamed(context, helpperRoute);
            }),
            drawerTitle(Icons.settings, 'Settings', () {
              Navigator.pop(context);
              Navigator.pushNamed(context, settingRoute);
            }),
            drawerTitle(Icons.info_sharp, 'About', () {
              Navigator.pop(context);
              Navigator.pushNamed(context, aboutRoute);
            }),
            
            drawerTitle(Icons.logout_sharp, 'logout', () {
              Navigator.pushNamedAndRemoveUntil(
                  context, loginRoute, (route) => false);
            }),
          ],
        ),
      ),
      body: Container(
        padding: EdgeInsets.all(30.0),
        child: GridView.count(
          crossAxisCount: 2,
          children: <Widget>[
            MyMenu("Product", Icons.shopping_basket, Colors.orange, () {
              Navigator.pushNamed(context, productRoute);
            }),
            MyMenu("Order cut", Icons.cut_sharp, Colors.indigo, () {
              Navigator.pushNamed(context, orderCutRoute);
            }),
             MyMenu(
              "History",
              Icons.history_sharp,
              Colors.brown,
              () {
                Navigator.pushNamed(context, historyRoute);
              },
            ),
             MyMenu(
              "Love",
              Icons.star_sharp,
              Colors.pink,
              () {
                Navigator.pushNamed(context, loveRoute);
              },
            ),
             MyMenu(
              "Profile",
              Icons.person_pin,
              Colors.blue,
              () {
                Navigator.pushNamed(context, profileRoute);
              },
            ),
            MyMenu(
              "Helpper",
              Icons.help_sharp,
              Colors.green,
              () {
                Navigator.pushNamed(context, helpperRoute);
              },
            ),
            MyMenu(
              "Setting",
              Icons.settings,
              Colors.lime,
              () {
                Navigator.pushNamed(context, settingRoute);
              },
            ),
             MyMenu(
              "About",
              Icons.info_sharp,
              Colors.purple,
              () {
                Navigator.pushNamed(context, aboutRoute);
              },
            ),
            
          ],
        ),
      ),
    );
  }
}

// Drawer Menu
class drawerTitle extends StatelessWidget {
  drawerTitle(this.ICON, this.Title, this.onTap);

  IconData ICON;
  String Title;
  var onTap;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(20.0, 0, 5, 0),
      child: Container(
        decoration: BoxDecoration(
          border: Border(bottom: BorderSide(color: Colors.teal)),
        ),
        child: InkWell(
          splashColor: Colors.teal,
          onTap: onTap,
          child: Container(
            height: 50.0,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Icon(
                      ICON,
                      color: Colors.teal,
                    ),
                    Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Text(
                        Title,
                        style: TextStyle(
                          fontSize: 18.0,
                        ),
                      ),
                    ),
                  ],
                ),
                // Icon(Icons.keyboard_arrow_right,
                //  color: Colors.teal,),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

//homeDashboard
class MyMenu extends StatelessWidget {
  String title;
  IconData icon;
  MaterialColor material_color;
  var onPressd;

  MyMenu(this.title, this.icon, this.material_color, this.onPressd);

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 10,
      shadowColor: Colors.indigo,
      margin: EdgeInsets.all(8.0),
      child: Ink(
        child: InkWell(
          splashColor: Colors.tealAccent,
          onTap: onPressd,
          child: Center(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Icon(
                  icon,
                  size: 80.0,
                  color: material_color,
                ),
                Text(title,
                    style: new TextStyle(fontSize: 20.0, fontFamily: "LaoFont"))
              ],
            ),
          ),
        ),
      ),
    );
  }
}
