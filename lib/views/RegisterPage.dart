import 'package:dadafashion_app/router/router_name.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:fluttertoast/fluttertoast.dart';

class RegisterPage extends StatefulWidget {
  @override
  _RegisterPageState createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  bool _obscureText = true;

  var _ctrlFullName = TextEditingController();
  var _ctrlTel = TextEditingController();
  var _ctrlEmail = TextEditingController();
  var _ctrlFacebook = TextEditingController();
  var _ctrlPassword = TextEditingController();
  var _ctrlVillage = TextEditingController();
  var _ctrlDistrict = TextEditingController();
  var _ctrlProvince = TextEditingController();
  var _ctrlCountry = TextEditingController();
  var sex, status;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: ListView(
          children: <Widget>[
            Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height / 5,
              decoration: BoxDecoration(
                  gradient: LinearGradient(
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    colors: [Colors.lightBlue, Colors.green],
                  ),
                  borderRadius:
                      BorderRadius.only(bottomRight: Radius.circular(100))),
              child: Stack(
                children: [
                  Align(
                    alignment: Alignment.center,
                    child: Container(
                      width: MediaQuery.of(context).size.width,
                      height: MediaQuery.of(context).size.height / 6.3,
                      child: Center(
                        child: SvgPicture.asset('images/newsletter.svg',
                            semanticsLabel: 'A red up arrow'),
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                        top: MediaQuery.of(context).size.height / 6.5,
                        left: 20),
                    child: Text(
                      'Create New Account',
                      style: TextStyle(color: Colors.white, fontSize: 30.0),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              height: MediaQuery.of(context).size.height -
                  MediaQuery.of(context).size.height / 4,
              width: MediaQuery.of(context).size.width,
              padding: EdgeInsets.only(
                  top: MediaQuery.of(context).size.height / 50,
                  left: 30,
                  right: 30),
              child: ListView(
                children: <Widget>[
                  TextFormField(
                    controller: _ctrlFullName,
                    decoration: InputDecoration(
                      icon: Icon(
                        Icons.person_sharp,
                        color: Colors.grey,
                      ),
                      hintText: 'Full Name',
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 10),
                    child: SingleChildScrollView(
                      child: Row(
                        children: [
                          Icon(
                            Icons.female_sharp,
                            color: Colors.grey,
                          ),
                          Text('   Sex: '),
                          Radio(
                            value: 'Male',
                            groupValue: sex,
                            onChanged: (value) {
                              setState(() {
                                sex = value;
                                print(sex);
                              });
                            },
                            activeColor: Colors.orange,
                          ),
                          Text("Male"),
                          Radio(
                            value: 'Female',
                            groupValue: sex,
                            onChanged: (value) {
                              setState(() {
                                sex = value;
                                print(sex);
                              });
                            },
                            activeColor: Colors.orange,
                          ),
                          Text("Female"),
                        ],
                      ),
                    ),
                  ),
                  TextFormField(
                    controller: _ctrlTel,
                    decoration: InputDecoration(
                      icon: Icon(
                        Icons.phone_sharp,
                        color: Colors.grey,
                      ),
                      hintText: 'Tel',
                    ),
                  ),
                  TextFormField(
                    controller: _ctrlEmail,
                    decoration: InputDecoration(
                      icon: Icon(
                        Icons.email_sharp,
                        color: Colors.grey,
                      ),
                      hintText: 'Email',
                    ),
                  ),
                  TextFormField(
                    controller: _ctrlFacebook,
                    decoration: InputDecoration(
                      icon: Icon(
                        Icons.facebook_sharp,
                        color: Colors.grey,
                      ),
                      hintText: 'Facebook',
                    ),
                  ),
                  TextFormField(
                    controller: _ctrlVillage,
                    decoration: InputDecoration(
                      icon: Icon(
                        Icons.home_filled,
                        color: Colors.grey,
                      ),
                      hintText: 'Village',
                    ),
                  ),
                  TextFormField(
                    controller: _ctrlDistrict,
                    decoration: InputDecoration(
                      icon: Icon(
                        Icons.location_history_sharp,
                        color: Colors.grey,
                      ),
                      hintText: 'District',
                    ),
                  ),
                  TextFormField(
                    controller: _ctrlProvince,
                    decoration: InputDecoration(
                      icon: Icon(
                        Icons.location_on_sharp,
                        color: Colors.grey,
                      ),
                      hintText: 'Province',
                    ),
                  ),
                  TextFormField(
                    controller: _ctrlCountry,
                    decoration: InputDecoration(
                      icon: Icon(
                        Icons.landscape_sharp,
                        color: Colors.grey,
                      ),
                      hintText: 'Country',
                    ),
                  ),
                  TextFormField(
                    obscureText: _obscureText,
                    controller: _ctrlPassword,
                    decoration: InputDecoration(
                      suffixIcon: GestureDetector(
                        onTap: () {
                          setState(() => _obscureText = !_obscureText);
                        },
                        child: Icon(_obscureText
                            ? Icons.visibility
                            : Icons.visibility_off),
                      ),
                      icon: Icon(
                        Icons.vpn_key,
                        color: Colors.grey,
                      ),
                      hintText: 'password',
                    ),
                  ),
                  Align(
                    alignment: Alignment.center,
                    child: Row(
                      children: [
                        Icon(
                          Icons.location_pin,
                          color: Colors.grey,
                        ),
                        Text('   Status: '),
                        Radio(
                          value: 'General',
                          groupValue: status,
                          onChanged: (value) {
                            setState(() {
                              status = value;
                              print(status);
                            });
                          },
                          activeColor: Colors.orange,
                        ),
                        Text('General'),
                        Radio(
                          value: 'Member',
                          groupValue: status,
                          onChanged: (value) {
                            setState(() {
                              status = value;
                              print(status);
                            });
                          },
                          activeColor: Colors.orange,
                        ),
                        Text('Member'),
                      ],
                    ),
                  ),
                  SizedBox(height: 20.0),
                  Padding(
                    padding: const EdgeInsets.only(left: 30, right: 30,bottom: 30),
                    child: Row(
                      children: <Widget>[
                        Expanded(
                          child: RaisedButton(
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(30)),
                            color: Colors.green,
                            textColor: Colors.white,
                            onPressed: () async {
                              Fluttertoast.showToast(
                                  msg: "Create New Account Success",
                                  toastLength: Toast.LENGTH_LONG,
                                  gravity: ToastGravity.CENTER,
                                  timeInSecForIosWeb: 1,
                                  backgroundColor: Colors.orange,
                                  textColor: Colors.white,
                                  fontSize: 16.0);
                              if(status=='Member'){
Navigator.pushNamed(
                                  context, memberRoute);
                              }else{
                                Navigator.pushNamedAndRemoveUntil(
                                  context, homeRoute, (route) => false);
                              }
                            },
                            padding: EdgeInsets.all(10),
                            child: Text(
                              'Next',
                              style: TextStyle(
                                  fontSize: 20,
                                  fontWeight: FontWeight.bold,
                                  fontFamily: 'LaoFont'),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
