
import 'package:dadafashion_app/router/router_name.dart';
import 'package:flutter/material.dart';

class AboutPage extends StatefulWidget {
  @override
  _AboutPageState createState() => _AboutPageState();
}

class _AboutPageState extends State<AboutPage> {
  @override
  Widget build(BuildContext context) {
   return Scaffold(
      appBar:  new AppBar(
        title: Text( "about"),
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back,
          ),
          onPressed: (){
            Navigator.pushNamedAndRemoveUntil(context, homeRoute,(route)=>false);
          },
        ),
      ),
      body:ListView(
        children: <Widget>[
           Container(
            child: Center(
              child: Text('ຂັ້ນຕອນທີ 1',style: TextStyle(fontSize: 18,color: Colors.deepOrange),),
            )
          ),
        ],
      ),
    );
  }
}
