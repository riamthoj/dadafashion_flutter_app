
import 'package:dadafashion_app/router/router_name.dart';
import 'package:flutter/material.dart';

class LovePage extends StatefulWidget {
  @override
  _LovePageState createState() => _LovePageState();
}

class _LovePageState extends State<LovePage> {
  @override
  Widget build(BuildContext context) {
   return Scaffold(
      appBar:  new AppBar(
        title: Text( "Love"),
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back,
          ),
          onPressed: (){
            Navigator.pushNamedAndRemoveUntil(context, homeRoute,(route)=>false);
          },
        ),
      ),
      body:ListView(
        children: <Widget>[
           Container(
            child: Center(
              child: Text('Love',style: TextStyle(fontSize: 18,color: Colors.deepOrange),),
            )
          ),
        ],
      ),
    );
  }
}
