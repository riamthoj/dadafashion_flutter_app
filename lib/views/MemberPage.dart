import 'package:dadafashion_app/router/router_name.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:fluttertoast/fluttertoast.dart';

class MemberPage extends StatefulWidget {
  @override
  _MemberPageState createState() => _MemberPageState();
}

class _MemberPageState extends State<MemberPage> {
  var _ctrlMoney = TextEditingController();
  var _ctrlDiscount = TextEditingController();
  var paymethod;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: ListView(
          children: <Widget>[
            Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height / 4,
              decoration: BoxDecoration(
                  gradient: LinearGradient(
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    colors: [Colors.green, Colors.green],
                  ),
                  borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(100),
                      bottomRight: Radius.circular(100))),
              child: Stack(
                children: [
                  Align(
                    alignment: Alignment.center,
                    child: Container(
                      width: MediaQuery.of(context).size.width,
                      height: MediaQuery.of(context).size.height / 5,
                      child: Center(
                        child: SvgPicture.asset('images/working_remotely.svg',
                            semanticsLabel: 'A red up arrow'),
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 20, left: 20),
                    child: Text(
                      'Us Account to pay money:',
                      style: TextStyle(color: Colors.white, fontSize: 18.0),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 50, left: 20),
                    child: Text(
                      'Email: riamthoj2021@gmail.com',
                      style: TextStyle(color: Colors.white, fontSize: 18.0),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 80, left: 20),
                    child: Text(
                      'Bcel One NO: 121 234 543 111 223',
                      style: TextStyle(color: Colors.white, fontSize: 18.0),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 110, left: 20),
                    child: Text(
                      'Tel: 020 76718747',
                      style: TextStyle(color: Colors.white, fontSize: 18.0),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              padding: EdgeInsets.only(
                  top: MediaQuery.of(context).size.height / 50,
                  left: 30,
                  right: 30),
              child: Column(
                children: <Widget>[
                  SizedBox(
                    height: 20,
                  ),
                  Card(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Row(
                          children: [
                            Text('Pay method: '),
                          ],
                        ),
                        Row(
                          children: [
                            Radio(
                              value: 'Bcel One',
                              groupValue: paymethod,
                              onChanged: (value) {
                                setState(() {
                                  paymethod = value;
                                  print(paymethod);
                                });
                              },
                              activeColor: Colors.orange,
                            ),
                            Text('Bcel One'),
                          ],
                        ),
                        Row(
                          children: [
                            Radio(
                              value: 'Paypal',
                              groupValue: paymethod,
                              onChanged: (value) {
                                setState(() {
                                  paymethod = value;
                                  print(paymethod);
                                });
                              },
                              activeColor: Colors.orange,
                            ),
                            Text('Paypal'),
                          ],
                        ),
                        Row(
                          children: [
                            Radio(
                              value: 'Money game',
                              groupValue: paymethod,
                              onChanged: (value) {
                                setState(() {
                                  paymethod = value;
                                  print(paymethod);
                                });
                              },
                              activeColor: Colors.orange,
                            ),
                            Text('Money game'),
                          ],
                        ),
                      ],
                    ),
                  ),
                  TextFormField(
                    controller: _ctrlMoney,
                    decoration: InputDecoration(
                      icon: Icon(
                        Icons.payments_sharp,
                        color: Colors.grey,
                      ),
                      hintText: 'Money pay',
                    ),
                  ),
                  TextFormField(
                    controller: _ctrlDiscount,
                    decoration: InputDecoration(
                      icon: Icon(
                        Icons.free_breakfast,
                        color: Colors.grey,
                      ),
                      hintText: 'Discount',
                    ),
                  ),
                ],
              ),
            ),
            Row(
              children: [
                Spacer(),
                Container(
                  width: 100,
                  height: MediaQuery.of(context).size.height / 7,
                  margin: EdgeInsets.only(
                    top: MediaQuery.of(context).size.height / 4.7,
                  ),
                  decoration: BoxDecoration(
                      gradient: LinearGradient(
                        begin: Alignment.topLeft,
                        end: Alignment.bottomRight,
                        colors: [Colors.grey, Colors.white70],
                      ),
                      borderRadius:
                          BorderRadius.only(topLeft: Radius.circular(100))),
                  child: Stack(
                    children: [
                      Align(
                        alignment: Alignment.bottomRight,
                        child: IconButton(
                          onPressed: () {
                            Fluttertoast.showToast(
                                msg: "Create New Account Success",
                                toastLength: Toast.LENGTH_LONG,
                                gravity: ToastGravity.CENTER,
                                timeInSecForIosWeb: 1,
                                backgroundColor: Colors.orange,
                                textColor: Colors.white,
                                fontSize: 16.0);
                            Navigator.pushNamedAndRemoveUntil(
                                context, homeRoute, (route) => false);
                          },
                          icon: Icon(Icons.save_sharp),
                          iconSize: 80,
                          color: Colors.teal,
                        ),
                      ),
                      Align(
                        alignment: Alignment.bottomCenter,
                        child: Text(
                          "submit",
                          style: TextStyle(color: Colors.teal, fontSize: 20),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
