
import 'package:dadafashion_app/router/router_name.dart';
import 'package:flutter/material.dart';

class CartPage extends StatefulWidget {
  @override
  _CartPageState createState() => _CartPageState();
}

class _CartPageState extends State<CartPage> {
  @override
  Widget build(BuildContext context) {
   return Scaffold(
      appBar:  new AppBar(
        title: Text( "Cart"),
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back,
          ),
          onPressed: (){
            Navigator.pushNamedAndRemoveUntil(context, homeRoute,(route)=>false);
          },
        ),
      ),
      body:ListView(
        children: <Widget>[
           Container(
            child: Center(
              child: Text('Cart',style: TextStyle(fontSize: 18,color: Colors.deepOrange),),
            )
          ),
        ],
      ),
    );
  }
}
