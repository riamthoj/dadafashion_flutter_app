import 'package:flutter/material.dart';

class NotFoundPage extends StatefulWidget {
  @override
  _NotFoundPageState createState() => _NotFoundPageState();
}

class _NotFoundPageState extends State<NotFoundPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Not Found  page'),
        ),
        body: Center(
            child: CircularProgressIndicator(
                value: .3,
                strokeWidth: 3,
                valueColor: AlwaysStoppedAnimation(Colors.orange),
                backgroundColor: Colors.tealAccent)));
  }
}
