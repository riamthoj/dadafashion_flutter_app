
import 'package:dadafashion_app/api/ProductAPI.dart';
import 'package:dadafashion_app/models/product.dart';
import 'package:dadafashion_app/router/router_name.dart';
import 'package:flutter/material.dart';

class ProductPage extends StatefulWidget {
  @override
  _ProductPageState createState() => _ProductPageState();
}

class _ProductPageState extends State<ProductPage> {
  ProductAPI? productAPI =ProductAPI();
  @override
  Widget build(BuildContext context) {
   return Scaffold(
      appBar:  new AppBar(
        title: Text( "Product"),
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back,
          ),
          onPressed: (){
            Navigator.pushNamedAndRemoveUntil(context, homeRoute,(route)=>false);
          },
        ),
      ),
      body:ListView(
        children: <Widget>[
           Container(
            child: Center(
              child: Text('Product',style: TextStyle(fontSize: 18,color: Colors.deepOrange),),
            )
          ),
          const Divider(),
                                FutureBuilder(
                                  future: productAPI!.getProducts(),
                                  builder: (context, AsyncSnapshot? snapshot) {
                                    List<ProductModel>? products = snapshot!.data;
                                    if (snapshot.hasData) {
                                      return Container(
                                        color: Colors.white,
                                        height: 370,
                                        width:
                                            MediaQuery.of(context).size.width,
                                        child: ListView.builder(
                                          itemCount: products!.length,
                                          itemBuilder: (context, index) {
                                            final price = NumberFormat(
                                                    "#,### ກີບ")
                                                .format(
                                                    products[index].price);
                                            final qtyFormat =
                                                NumberFormat("#,###").format(
                                                    products[index].qty);

                                            int? checkQty =
                                                products[index].qty!.toInt();
                                            return ListTile(
                                              dense: true,
                                              contentPadding:
                                                  const EdgeInsets.only(
                                                      left: 3, right: 8),
                                              subtitle: Column(
                                                children: [
                                                  SizedBox(
                                                    height: index != 0 ? 4 : 6,
                                                  ),
                                                  Container(
                                                    color: Colors.white,
                                                    width:
                                                        MediaQuery.of(context)
                                                            .size
                                                            .width,
                                                    child: Row(
                                                      mainAxisSize:
                                                          MainAxisSize.max,
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .spaceBetween,
                                                      // ignore: prefer_const_literals_to_create_immutables
                                                      children: [
                                                        Expanded(
                                                          child: Text(
                                                            products[index]
                                                                .productId
                                                                .toString(),
                                                            style:
                                                                const TextStyle(
                                                              fontFamily:
                                                                  familyFont,
                                                              color: fontColor,
                                                            ),
                                                          ),
                                                        ),
                                                        Expanded(
                                                          child: Text(
                                                            products[index]
                                                                .productName!,
                                                            style:
                                                                const TextStyle(
                                                              fontFamily:
                                                                  familyFont,
                                                              color: fontColor,
                                                            ),
                                                          ),
                                                        ),
                                                        Expanded(
                                                          child: Text(
                                                            products[index]
                                                                .category!,
                                                            style:
                                                                const TextStyle(
                                                              fontFamily:
                                                                  familyFont,
                                                              color: fontColor,
                                                            ),
                                                          ),
                                                        ),
                                                        Expanded(
                                                          child: Text(
                                                            wholeFormat,
                                                            style:
                                                                const TextStyle(
                                                              fontFamily:
                                                                  familyFont,
                                                              color: fontColor,
                                                            ),
                                                          ),
                                                        ),
                                                        Expanded(
                                                          child: Text(
                                                            retailFormat,
                                                            style:
                                                                const TextStyle(
                                                              fontFamily:
                                                                  familyFont,
                                                              color: fontColor,
                                                            ),
                                                          ),
                                                        ),
                                                        Expanded(
                                                          child:
                                                              (checkQty >
                                                                      constQty!)
                                                                  ? Text(
                                                                      qtyFormat,
                                                                      style:
                                                                          const TextStyle(
                                                                        fontFamily:
                                                                            familyFont,
                                                                        color:
                                                                            fontColor,
                                                                      ),
                                                                    )
                                                                  : Text(
                                                                      qtyFormat,
                                                                      style:
                                                                          const TextStyle(
                                                                        fontFamily:
                                                                            familyFont,
                                                                        color:
                                                                            errorColor,
                                                                        fontWeight:
                                                                            FontWeight.bold,
                                                                        fontSize:
                                                                            24,
                                                                      ),
                                                                    ),
                                                        ),
                                                        SizedBox(
                                                          width: 200,
                                                          child: Row(
                                                            children: [
                                                              // ignore: deprecated_member_use
                                                              RaisedButton(
                                                                color: Colors
                                                                    .orange,
                                                                onPressed: () {
                                                                  // editDialog(
                                                                  //     id: mytests[
                                                                  //             index]
                                                                  //         .id);
                                                                },
                                                                child: Row(
                                                                  // ignore: prefer_const_literals_to_create_immutables
                                                                  children: [
                                                                    const Icon(
                                                                      FontAwesomeIcons
                                                                          .pen,
                                                                      color: Colors
                                                                          .white,
                                                                      size: 16,
                                                                    ),
                                                                    const SizedBox(
                                                                      width: 5,
                                                                    ),
                                                                    const Text(
                                                                      "ແກ້ໄຂ",
                                                                      style:
                                                                          TextStyle(
                                                                        color: Colors
                                                                            .white,
                                                                        fontFamily:
                                                                            familyFont,
                                                                      ),
                                                                    ),
                                                                  ],
                                                                ),
                                                              ),
                                                              const SizedBox(
                                                                width: 20,
                                                              ),
                                                              // ignore: deprecated_member_use
                                                              RaisedButton(
                                                                color:
                                                                    Colors.red,
                                                                onPressed: () {
                                                                  warnningAlter(
                                                                      context:
                                                                          context,
                                                                      onPressed:
                                                                          () {});
                                                                },
                                                                child: Row(
                                                                  // ignore: prefer_const_literals_to_create_immutables
                                                                  children: [
                                                                    const Icon(
                                                                      FontAwesomeIcons
                                                                          .trash,
                                                                      color: Colors
                                                                          .white,
                                                                      size: 16,
                                                                    ),
                                                                    const SizedBox(
                                                                      width: 5,
                                                                    ),
                                                                    const Text(
                                                                      "ລຶບ",
                                                                      style:
                                                                          TextStyle(
                                                                        color: Colors
                                                                            .white,
                                                                        fontFamily:
                                                                            familyFont,
                                                                      ),
                                                                    ),
                                                                  ],
                                                                ),
                                                              ),
                                                            ],
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            );
                                          },
                                        ),
                                      );
                                    } else if (snapshot.hasError) {
                                      return const Text("Error");
                                    }
                                    return const Center(
                                      child: CircularProgressIndicator(),
                                    );
                                  },
                                ),
        ],
      ),
    );
  }
}
