
import 'package:dadafashion_app/router/router_name.dart';
import 'package:flutter/material.dart';

class OrderCutPage extends StatefulWidget {
  @override
  _OrderCutPageState createState() => _OrderCutPageState();
}

class _OrderCutPageState extends State<OrderCutPage> {
  @override
  Widget build(BuildContext context) {
   return Scaffold(
      appBar:  new AppBar(
        title: Text( "OrderCute"),
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back,
          ),
          onPressed: (){
            Navigator.pushNamedAndRemoveUntil(context, homeRoute,(route)=>false);
          },
        ),
      ),
      body:ListView(
        children: <Widget>[
           Container(
            child: Center(
              child: Text('Order Cut',style: TextStyle(fontSize: 18,color: Colors.deepOrange),),
            )
          ),
        ],
      ),
    );
  }
}
