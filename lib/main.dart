import 'package:dadafashion_app/router/custome_router.dart';
import 'package:dadafashion_app/router/router_name.dart';
import 'package:dadafashion_app/views/LoginPage.dart';
import 'package:flutter/material.dart';


void main() => runApp(LoginApp());
class LoginApp extends StatefulWidget {
  @override
  _LoginAppState createState() => _LoginAppState();
}
class _LoginAppState extends State<LoginApp> {
  
  @override
  Widget build(BuildContext context) {
      return MaterialApp(
        title: 'DOSS Flutter',
        theme: ThemeData(
          primarySwatch: Colors.orange,
        ),
        debugShowCheckedModeBanner: false,
       home: LoginPage(),
        onGenerateRoute: CustomRouter.allRoutes,
        initialRoute: loginRoute
      );
  }
}
