import 'package:dadafashion_app/router/router_name.dart';
import 'package:dadafashion_app/views/AboutPage.dart';
import 'package:dadafashion_app/views/CartPage.dart';
import 'package:dadafashion_app/views/HelpperPage.dart';
import 'package:dadafashion_app/views/HistoryPage.dart';
import 'package:dadafashion_app/views/HomePage.dart';
import 'package:dadafashion_app/views/LoginPage.dart';
import 'package:dadafashion_app/views/LovePage.dart';
import 'package:dadafashion_app/views/MemberPage.dart';
import 'package:dadafashion_app/views/NotFoundPage.dart';
import 'package:dadafashion_app/views/NotificatPage.dart';
import 'package:dadafashion_app/views/OrderCutPage.dart';
import 'package:dadafashion_app/views/ProductPage.dart';
import 'package:dadafashion_app/views/ProfilePage.dart';
import 'package:dadafashion_app/views/RegisterPage.dart';
import 'package:dadafashion_app/views/SettingPage.dart';
import 'package:flutter/material.dart';
// import 'package:dadafashion_app/views/LoginPage.dart';
// import 'package:dadafashion_app/views/AboutPage.dart';


class CustomRouter {
  static Route<dynamic> allRoutes (RouteSettings settings) {
    switch (settings.name) {
      case loginRoute:
        return MaterialPageRoute(builder: (_) => LoginPage());
      case registerRoute:
        return MaterialPageRoute(builder: (_) => RegisterPage());
      case homeRoute:
        return MaterialPageRoute(builder: (_) => HomePage());
        case productRoute:
        return MaterialPageRoute(builder: (_) => ProductPage());
      case orderCutRoute:
        return MaterialPageRoute(builder: (_) => OrderCutPage());
      case settingRoute:
        return MaterialPageRoute(builder: (_) => SettingPage());
      case helpperRoute:
        return MaterialPageRoute(builder: (_) => HelpperPage());
      case aboutRoute:
        return MaterialPageRoute(builder: (_) => AboutPage());
      case memberRoute:
        return MaterialPageRoute(builder: (_) => MemberPage());
      case profileRoute:
        return MaterialPageRoute(builder: (_) => ProfilePage());
      case historyRoute:
        return MaterialPageRoute(builder: (_) => HistoryPage());
      case loveRoute:
        return MaterialPageRoute(builder: (_) => LovePage());
      case cartRoute:
        return MaterialPageRoute(builder: (_) => CartPage());
      case notificatRoute:
        return MaterialPageRoute(builder: (_) => NotificatPage());
    }
    return MaterialPageRoute(builder: (_) => NotFoundPage());
  }
}
